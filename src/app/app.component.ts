import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {



  
  title = 'blog';
  datePost = new Date();
  posts = [
    {
      title: 'Post N°1',
      content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi maximus justo nec condimentum maximus. Sed id ornare ipsum. Sed volutpat ante ligula, sed cursus nisi convallis sit amet. Pellentesque a ante tortor. Curabitur bibendum scelerisque risus in commodo. In condimentum quam ac iaculis placerat. Nunc efficitur urna dui, a auctor est cursus nec. Nulla in interdum felis. Pellentesque in urna lacus.',
      loveIts : 0,
      created_at: this.datePost
    },
    {
      title: 'Post N°2',
      content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi maximus justo nec condimentum maximus. Sed id ornare ipsum. Sed volutpat ante ligula, sed cursus nisi convallis sit amet. Pellentesque a ante tortor. Curabitur bibendum scelerisque risus in commodo. In condimentum quam ac iaculis placerat. Nunc efficitur urna dui, a auctor est cursus nec. Nulla in interdum felis. Pellentesque in urna lacus.',
      loveIts : 2,
      created_at: this.datePost
    },
    {
      title: 'Post N°3',
      content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi maximus justo nec condimentum maximus. Sed id ornare ipsum. Sed volutpat ante ligula, sed cursus nisi convallis sit amet. Pellentesque a ante tortor. Curabitur bibendum scelerisque risus in commodo. In condimentum quam ac iaculis placerat. Nunc efficitur urna dui, a auctor est cursus nec. Nulla in interdum felis. Pellentesque in urna lacus.',
      loveIts : -1,
      created_at: this.datePost
    }
  ];



  

  constructor() {
    
  }
}
