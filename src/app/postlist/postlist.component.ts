import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-postlist',
  templateUrl: './postlist.component.html',
  styleUrls: ['./postlist.component.scss']
})
export class PostlistComponent implements OnInit {
@Input() postTitle: string;
@Input() postContent: string;
@Input() postLoveIts: number;
@Input() postCreatedAt: Date;

  constructor() {
    
   }
addPoint() {
  this.postLoveIts++;

}
lessPoint() {
  this.postLoveIts--;
}
  ngOnInit() {
  }

}
